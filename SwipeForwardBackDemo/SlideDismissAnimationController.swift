//
//  SlideDismissAnimationController.swift
//  SwipeForwardBackDemo
//  Copyright © 2019 john. All rights reserved.
//

import UIKit

class SlideDismissAnimationController: NSObject, UIViewControllerAnimatedTransitioning{
    
    private let destinationFrame: CGRect
    let interactionController: SlideInteractionController?
    
    init(destinationFrame: CGRect, interactionController: SlideInteractionController?) {
        self.destinationFrame = destinationFrame
        self.interactionController = interactionController
    }
    
    
    func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        return 0.7
    }
    
    
    func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        
        // Get a local reference to the from and to VCs and a snapshot of what the screen will look like after the transition (the toVC appearance).
        guard let fromVC = transitionContext.viewController(forKey: .from), let toVC = transitionContext.viewController(forKey: .to), let snapshot = toVC.view.snapshotView(afterScreenUpdates: true) else{
            return
        }
        
        // Get the container view from transitionContext where animation is performed.
        let container = transitionContext.containerView
        
        // Get sizes of the VCs at the end of the transition from the context
        toVC.view.frame = transitionContext.finalFrame(for: toVC)
        fromVC.view.frame = transitionContext.finalFrame(for: fromVC)
        
        // Setup the transforms
        let offScreenRight = CGAffineTransform(translationX: container.frame.width, y: 0)
        let offScreenLeft = CGAffineTransform(translationX: -container.frame.width/10, y: 0)
        
        // start the toView to the left side of the visible animation area 1/10 the width of the container
        toVC.view.transform = offScreenLeft
        
        // add both views for animation to the container
        container.addSubview(toVC.view)
        container.addSubview(fromVC.view)
        
        // Get the setting for duration from above
        let duration = self.transitionDuration(using: transitionContext)
        
        // Start the actual animation
        UIView.animate(withDuration: duration,
                       delay: 0.0,
                       usingSpringWithDamping: 0.9,
                       initialSpringVelocity: 0.8,
                       options: .curveEaseOut,
                       animations: {
                        
                        // animate the currently visible VC off to the right
                        fromVC.view.transform = offScreenRight
                        
                        // return the VC we want visible now to it's original size/transformation
                        toVC.view.transform = CGAffineTransform.identity// CGAffineTransformIdentity
        }) { finished in
            
            
            // return both VCs to their original placement and size
            toVC.view.transform = CGAffineTransform.identity
            fromVC.view.transform = CGAffineTransform.identity
            
            // If SlideInteractionController is enabled and the animation is manually cancelled
            // make sure to remove the toVC otherwise it'll remain visible
            if transitionContext.transitionWasCancelled {
                toVC.view.removeFromSuperview()
            }
            
            // Tell context the animation is complete -  based on whether the transitionContext cancelled
            transitionContext.completeTransition(!transitionContext.transitionWasCancelled)
        }
        
    }
    
}

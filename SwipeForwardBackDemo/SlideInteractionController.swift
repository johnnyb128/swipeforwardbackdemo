//
//  SlideInteractionController.swift
//  SwipeForwardBackDemo
//  Copyright © 2019 john. All rights reserved.
//

import UIKit

class SlideInteractionController: UIPercentDrivenInteractiveTransition{
    
    var interactionInProgress = false
    
    private var shouldCompleteTransition = false
    private weak var viewController: UIViewController!
    
    init(viewController: UIViewController) {
        super.init()
        self.viewController = viewController
        
        // Setup the dismiss gesture and add it to the incoming ViewController
        let leftGesture = UIScreenEdgePanGestureRecognizer(target: self, action: #selector(handleGesture(_:)))
        leftGesture.edges = .left
        viewController.view.addGestureRecognizer(leftGesture)
        
        // Setup the dismiss gesture and add it to the incoming ViewController
        let rightGesture = UIScreenEdgePanGestureRecognizer(target: self, action: #selector(handleGesture(_:)))
        rightGesture.edges = .right
        viewController.view.addGestureRecognizer(rightGesture)
        
    }
    

    @objc func handleGesture(_ gestureRecognizer: UIScreenEdgePanGestureRecognizer) {
        
        let translation = gestureRecognizer.translation(in: gestureRecognizer.view!.superview!)
        
        var progress:CGFloat = 0.0
        
        if gestureRecognizer.edges == .right{
            // Swiping in from the right is a negative translation.x so get absolute value
            progress =  (abs(translation.x) / 400.0)
        }else{
            //Dividing by a high number reduces the progress and keeps the animation
            //in-sync with the user finger.
            progress =  (translation.x / 1000.0)
        }
       
        progress = CGFloat(fminf(fmaxf(Float(progress), 0.0), 1.0))
        
        // Switch the state of the gesture to determine what action to take
        switch gestureRecognizer.state {
            
        case .began:
            
            // Forward transition
            if gestureRecognizer.edges == .right{
                interactionInProgress = true
                setupAndPresentNextViewController()

            // Back transition
            }else{
                interactionInProgress = true
                viewController.dismiss(animated: true, completion: nil)
            }
           
        case .changed:
            shouldCompleteTransition = progress > 0.35
            update(progress)
            
        case .cancelled:
            interactionInProgress = false
            cancel()
        
        case .ended:
            interactionInProgress = false
            if shouldCompleteTransition {
                finish()
            } else {
                cancel()
                
            }
        default:
            break
        }
        
        
    }
    
    /**
     Takes the currently presented VC, decides if it's First or Second VC, then
     creates the forward transition VC based on the current one.
     */
    private func setupAndPresentNextViewController(){
        
        interactionInProgress = true
    
        if let vc = viewController as? FirstViewController{
            
            let secondVC = (UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "secondviewcontroller") as? SecondViewController)!
            secondVC.transitioningDelegate = vc
            
            viewController.present(secondVC, animated: true, completion:nil)
            
        }else if let vc = viewController as? SecondViewController{
            
            let thirdVC = (UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "thirdviewcontroller") as? ThirdViewController)!
            thirdVC.transitioningDelegate = vc
           
            viewController.present(thirdVC, animated: true, completion:nil)
        }
    }
}

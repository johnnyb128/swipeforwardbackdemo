//
//  ViewController.swift
//  SwipeForwardBackDemo
//
//  Created by john on 8/11/19.
//  Copyright © 2019 john. All rights reserved.
//

import UIKit

class FirstViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }


}


// MARK: TransitioningDelegate
extension FirstViewController:UIViewControllerTransitioningDelegate{
    
    
    func animationController(forPresented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning?{
        return SlidePresentAnimationController(originFrame: self.view.frame)
    }
    
    
    func animationController(forDismissed dismissed: UIViewController)
        -> UIViewControllerAnimatedTransitioning? {
            
            var t_slideInteractionController: SlideInteractionController? = nil
            
            if let dismissedVC = dismissed as? SecondViewController {
                t_slideInteractionController = dismissedVC.slideInteractionController
            }
            
            return SlideDismissAnimationController(destinationFrame: self.view.frame,
                                                   interactionController: t_slideInteractionController)
    }
    
    
    func interactionControllerForDismissal(using animator: UIViewControllerAnimatedTransitioning)
        -> UIViewControllerInteractiveTransitioning? {
            
            guard let animator = animator as? SlideDismissAnimationController,
                let interactionController = animator.interactionController,
                interactionController.interactionInProgress
                else {
                    return nil
            }
            
            
            return interactionController
    }
    
    
    
}



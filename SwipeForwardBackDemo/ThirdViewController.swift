//
//  ThirdViewController.swift
//  SwipeForwardBackDemo
//  Copyright © 2019 john. All rights reserved.
//

import UIKit

class ThirdViewController: UIViewController {

    var slideInteractionController: SlideInteractionController?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Setup manual slide transition controller
        self.slideInteractionController = SlideInteractionController(viewController: self)
        
    }
    
}

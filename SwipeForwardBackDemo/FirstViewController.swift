//
//  FirstViewController.swift
//  SwipeForwardBackDemo
//  Copyright © 2019 john. All rights reserved.
//

import UIKit


class FirstViewController: UIViewController {
    
    var slideInteractionController: SlideInteractionController?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Setup manual slide transition controller
        self.slideInteractionController = SlideInteractionController(viewController: self)
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if let svc = segue.destination as? SecondViewController{
            svc.transitioningDelegate = self
        }
    }
    
    
    @IBAction func invokeSecondViewController(_ sender: Any) {
        
        performSegue(withIdentifier: "swipesegue", sender: self)
    }
    
}


// MARK: UIViewControllerTransitioningDelegate
extension FirstViewController:UIViewControllerTransitioningDelegate{
    
    
    func animationController(forPresented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning?{
        
        return SlidePresentAnimationController(originFrame: self.view.frame, interactionController: self.slideInteractionController)
    
    }
    
    // Return the Present Animator
    func animationController(forDismissed dismissed: UIViewController)
        -> UIViewControllerAnimatedTransitioning? {
            
            var t_slideInteractionController: SlideInteractionController? = nil
            
            // Only set the interaction controller if the incoming "dismissed" ViewController is SecondViewController
            if let dismissedVC = dismissed as? SecondViewController {
                t_slideInteractionController = dismissedVC.slideInteractionController
            }
            
            return SlideDismissAnimationController(destinationFrame: self.view.frame,
                                                   interactionController: t_slideInteractionController)
    }
    
     // Return the Dismiss Animator
    func interactionControllerForDismissal(using animator: UIViewControllerAnimatedTransitioning)
        -> UIViewControllerInteractiveTransitioning? {
            
            guard let animator = animator as? SlideDismissAnimationController,
                let interactionController = animator.interactionController,
                interactionController.interactionInProgress
                else {
                    return nil
            }
            
            
            return interactionController
    }
    
    
    func interactionControllerForPresentation(using animator: UIViewControllerAnimatedTransitioning) -> UIViewControllerInteractiveTransitioning? {
        
        // The SlideDismissAnimationController has our custom SlideInteractionController
        // set when it's initialized.  Get that and return it to UIKit so it can act
        // upon the user interaction when UIKit calls the delegate functions.
        guard let animator = animator as? SlidePresentAnimationController,
            let interactionController = animator.interactionController,
            interactionController.interactionInProgress
            else {
                return nil
        }
        
        
        return interactionController
    }
    
    
    
}

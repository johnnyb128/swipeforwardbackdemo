//
//  SlidePresentAnimationController.swift
//  SwipeForwardBackDemo
//  Copyright © 2019 john. All rights reserved.
//

import UIKit

class SlidePresentAnimationController: NSObject, UIViewControllerAnimatedTransitioning{
    
    private let originFrame: CGRect
    let interactionController: SlideInteractionController?
    
    init(originFrame: CGRect, interactionController: SlideInteractionController?) {
        self.originFrame = originFrame
        self.interactionController = interactionController
    }
    

    func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        return 1.5
    }
    
    

    func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        
        // Get a local reference to the from and to VCs and a snapshot of what the screen will look like after the transition (the toVC appearance).
        guard let fromVC = transitionContext.viewController(forKey: .from), let toVC = transitionContext.viewController(forKey: .to), let snapshot = toVC.view.snapshotView(afterScreenUpdates: true) else{
            return
        }
        
        // Get the container view from transitionContext where animation is performed.
        let container = transitionContext.containerView
        
        
        // Setup the transform that will shrink the VCs
        let shrink = CGAffineTransform.init(scaleX: 0.8, y: 0.8)
       
        // start the toVC to the right of the visible screen
        toVC.view.center.x += container.frame.width
        
        // shrink the toVC that's off to the right
        toVC.view.transform = shrink
        
        // add  both views for animation to the container
        container.addSubview(fromVC.view)
        container.addSubview(toVC.view)
        
        // Get the setting for duration from above
        let duration = self.transitionDuration(using: transitionContext)
        
        // Start the actual animation
        UIView.animateKeyframes(
            withDuration: duration,
            delay: 0,
            options: .calculationModeLinear,
            animations: {
                
                UIView.addKeyframe(withRelativeStartTime: 0.0, relativeDuration: 0.25, animations: {
                    // Shrink the currently visible VC
                    fromVC.view.transform = shrink
                })
                
                UIView.addKeyframe(withRelativeStartTime: 0.25, relativeDuration: 0.25, animations: {
                    
                    // move the currently visible VC off screen to the left
                    fromVC.view.center.x -= container.frame.width
                    
                    // move the shrunk toVC that's off to the right of screen back to center
                    toVC.view.center.x -= container.frame.width
                })
                
                UIView.addKeyframe(withRelativeStartTime: 0.5, relativeDuration: 0.5, animations: {
                    
                    // return toVC to it's original size
                    toVC.view.transform = CGAffineTransform.identity
                })
                
                        
                        
        }, completion:{_ in
            
                // return both VCs to their original placement and size
                toVC.view.transform = CGAffineTransform.identity
                fromVC.view.transform = CGAffineTransform.identity
            
            // If SlideInteractionController is enabled and the animation is manually cancelled
            // make sure to remove the toVC otherwise it'll remain visible
            if transitionContext.transitionWasCancelled {
                fromVC.view.removeFromSuperview()
            }
            
            // Tell context the animation is complete -  based on whether the transitionContext cancelled
            transitionContext.completeTransition(!transitionContext.transitionWasCancelled)
            })

    }
    
}
